'use strict'

const Schema = use('Schema')

class AlarmSchema extends Schema {
  up () {
    this.create('alarms', (table) => {
      table.increments()
      table.string('slug').notNullable()
      table.integer('user_id').unsigned().notNullable()
      table.string('name').notNullable()
      table.string('day').notNullable()
      table.string('time').notNullable()
      table.text('description').notNullable()
      table.string('utc').notNullable()
      table.boolean('active').defaultTo(true)
      table.timestamps()
    })
  }

  down () {
    this.drop('alarms')
  }
}

module.exports = AlarmSchema

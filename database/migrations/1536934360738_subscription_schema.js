'use strict'

const Schema = use('Schema')

class SubscriptionSchema extends Schema {
  up () {
    this.create('subscriptions', (table) => {
      table.increments()
      table.integer('user_id').unsigned().notNullable()
      table.string('endpoint', 500).notNullable()
      table.string('expirationTime').notNullable()
      table.string('p256dh', 500).notNullable()
      table.string('auth').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('subscriptions')
  }
}

module.exports = SubscriptionSchema

'use strict'

const Subscription = use('App/Models/Subscription')

class SubscriptionController {
  async store ({ request, response, auth }) {
    // Get Current User
    const user = auth.current.user
    // Get Subscription Data
    let { endpoint, expirationTime, keys } = request.body
    expirationTime = expirationTime === null ? '' : expirationTime
    console.log(expirationTime)
    try {
      const subscriptionExists = await Subscription.query().where({ user_id: user.id }).where({ endpoint }).getCount()
      if (subscriptionExists) return response.json({
        status: 'success',
        message: 'Already subscribed'
      })

      await Subscription.create({
        user_id: user.id,
        endpoint,
        expirationTime,
        ...keys
      })

      return response.json({
        status: 'success'
      })

    } catch (error) {
      console.log(error)
      return response.status(400).json({
        status: 'error',
        message: error
      })
    }
  }
}

module.exports = SubscriptionController

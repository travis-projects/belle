'use strict'

const Alarm = use('App/Models/Alarm')
const { validate } = use('Validator')
const slugify = require('slugify')
const randomize = require('randomatic')
const date = require('date-and-time')

class AlarmController {
  async index ({ auth, view }) {
    // Get Current User
    const user = auth.current.user
    const alarms = await Alarm.query().where({ user_id: user.id }).orderBy('id', 'desc').fetch()
    return view.render('alarm.index', {
      heroTitle: 'All Alarms',
      alarms: alarms.toJSON()
    })
  }

  create ({ view }) {
    return view.render('alarm.create')
  }

  async show ({ auth, params: { id: slug }, view, response }) {
    // Get Current User
    const user = auth.current.user
    try {
      const alarm = await Alarm.query().where({ user_id: user.id }).where({ slug }).firstOrFail()
      return view.render('alarm.show', { alarm })
    } catch (error) {
      return response.redirect('back')
    }
  }

  async store ({ request, auth, session, response }) {
    // Get Current User
    const user = auth.current.user
    // Get Alarm Data
    let alarmData = request.only([ 'name', 'description', 'day', 'time', 'fullTime' ])
    // Validate Data
    const validation = await validate(alarmData, {
      name: 'required|min:3|max:60',
      day: 'required',
      time: 'required',
      description: 'max:100'
    })

    if (validation.fails()) {
      session.withErrors(validation.messages()).flashAll()
      return response.redirect('back')
    }

    // UTC Actual
    const d = new Date(alarmData.fullTime)
    const utc = date.format(d, 'DD MMMM YYYY H:mm', true)
    delete alarmData.fullTime
    alarmData.utc = utc

    // Create Slug
    const slug = slugify(`${alarmData.name}-${randomize('0', 10)}`)
    alarmData.slug = slug

    // Create Alarm
    await Alarm.create({
      user_id: user.id,
      ...alarmData
    })
    // Create flash message, and redirect to alarms page
    session.flash({ notification: 'Alarm Added!' })
    return response.redirect(`/alarms/`)
  }

  async edit ({ params, view }) {
    const alarm = await Alarm.query().where({ id: params.id }).firstOrFail()
    return view.render('alarm.edit', { alarm })
  }

  async status ({ params: { slug, action }, response }) {
    // Check if enable/disable
    const active = action === 'disable' ? false : true
    // Update Alarm status
    await Alarm.query().where({ slug }).update({ active })
    return response.redirect('back')
  }

  async update ({ params: { id }, request, session, response }) {
    // Get Alarm Data
    const alarmData = request.only([ 'name', 'description', 'day', 'time', 'fullTime' ])
    // Validate Data
    const validation = await validate(alarmData, {
      name: 'required|min:3|max:60',
      day: 'required',
      time: 'required',
      description: 'max:100'
    })

    if (validation.fails()) {
      session.withErrors(validation.messages()).flashAll()
      return response.redirect('back')
    }

    // UTC Actual
    const d = new Date(alarmData.fullTime)
    const utc = date.format(d, 'DD MMMM YYYY H:mm', true)
    delete alarmData.fullTime
    alarmData.utc = utc

    await Alarm.query().where({ id }).update(alarmData)
    session.flash({ notification: 'Alarm Updated!' })
    return response.redirect(`/alarms/${id}`)
  }

  async destroy ({ params: { id }, session, response }) {
    await Alarm.query().where({ id }).delete()
    session.flash({ notification: 'Alarm Deleted!' })
    return response.redirect(`/alarms/`)
  }
}

module.exports = AlarmController

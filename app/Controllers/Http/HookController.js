'use strict'

const { checkTimes} = use('App/Services/Alarms')

class HookController {
  async index ({ request, response }) {
    const token = this.getToken(request)
    if (!token) return response.status(401).send('No Authentication')
    if (token !== process.env.HOOK_KEY) return response.status(403).send('Not Authorized to access this endpoint')
    const result = await checkTimes()
    return response.status(200).json(result)
  }

  getToken (request) {
    if (!request.headers().authorization) return null
    return request.headers().authorization
  }
}

module.exports = HookController

'use strict'

const User = use('App/Models/User')
const { validateAll } = use('Validator')

class UserController {

  async signup ({ request, session, auth, response }) {
    // Get User Data
    const userData = request.only([ 'firstname', 'lastname', 'email', 'password' ])
    // Validate Form Input
    const validation = await validateAll(userData, {
      firstname: 'required',
      lastname: 'required',
      email: 'required|email|unique:users',
      password: 'required'
    })
    // If fail, show errors
    if (validation.fails()) {
      session.withErrors(validation.messages()).flashExcept(['password'])
      return response.redirect('back')
    }
    // Create User
    const user = await User.create(userData)
    // Login User
    await auth.remember(true).login(user)
    // Redirect User to dashboard
    return response.redirect('/alarms')
  }

  async login ({ request, auth, session, response }) {
    // Get User Data
    const { email, password } = request.only(['email', 'password'])
    try {
      // Validate the user credentials and generate a Session
      await auth.remember(true).attempt(email, password)
      return response.redirect('/')
    } catch (error) {
      console.log(error)
      session.withErrors(error).flashExcept(['password'])
      return response.redirect('back')
    }

    console.log(token)
  }
}

module.exports = UserController

'use strict'

const Model = use('Model')

class Alarm extends Model {
  // User Relationship
  user () {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = Alarm

'use strict'

const Model = use('Model')

class Subscription extends Model {
  // User Relationship
  user () {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = Subscription

'use strict'

const date = require('date-and-time')
const webpush = require('web-push')

const vapidKeys = {
  publicKey: process.env.VAPID_PUBLIC_KEY,
  privateKey: process.env.VAPID_PRIVATE_KEY
}

webpush.setVapidDetails(
  'mailto:travis@cirruscreative.co.uk',
  vapidKeys.publicKey,
  vapidKeys.privateKey
)

const triggerPushMsg = (subscription, dataToSend) => {
  const payload = JSON.stringify(dataToSend)
  webpush.sendNotification(subscription, payload)
    .catch((err) => {
      if (err.statusCode === 410) {
        return deleteSubscriptionFromDatabase(subscription._id)
      } else {
        console.log('Subscription is no longer valid: ', err)
      }
    })
}

class CurrentTimeClass {
  constructor () { this.now = new Date() }
  get time() { return date.format(this.now, 'H:mm', true) }
  get day() { return date.format(this.now, 'DD MMMM, YYYY', true) }
  get utc() { return date.format(this.now, 'DD MMMM YYYY HH:mm', true) }
}

const checkTimes = async () => {
  // Setup now
  const { utc } = new CurrentTimeClass()
  // Fetch Alarms
  const Alarm = use('App/Models/Alarm')
  const alarms = await Alarm.query().where({ utc, active: true }).with('user.subscriptions').fetch()
  // Loop through alarms
  alarms.toJSON().forEach(async alarm => {
    // Edit the subscription object to the correct format needed by web-push
    const subscriptions = await alarm.user.subscriptions.map(subscription => ({
      endpoint: subscription.endpoint,
      keys: {
        p256dh: subscription.p256dh,
        auth: subscription.auth
      }
    }))
    // Run through subscriptions, and trigger web-push
    await subscriptions.forEach(subscription => triggerPushMsg(subscription, { alarm: alarm.name, description: alarm.description}))
  })
  return 'Done Alarms'
}

module.exports = { checkTimes }

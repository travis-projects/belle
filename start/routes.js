'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')

// ! API ROUTES
// Auth Routes
Route.group(() => {
  Route.post('/signup', 'UserController.signup')
  Route.post('/login', 'UserController.login')
}).prefix('auth')
// Alarms Routes
Route.resource('alarms', 'AlarmController').middleware(['authenticated', 'auth'])
// Activate/Disable Alarms
Route.post('/alarm/:slug/:action', 'AlarmController.status')
// Subscription Routes
Route.post('/subscription', 'SubscriptionController.store').middleware(['auth'])
Route.get('/hook', 'HookController.index')

// ! VIEW ROUTES
Route.group(() => {
  // Home Route
  Route.on('/').render('home').as('home')
  // Display Auth Forms
  Route.on('login').render('user/login').as('login')
  Route.on('signup').render('user/signup').as('signup')
})

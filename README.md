# Belle

Fullstack AdonisJS app for timers/reminders, using browser notifications.

It uses a hosted MySQL database, and is hosted on Zeit Now.
It also uses the Edge template engine for the frontend.

### Developing

```
cd belle
yarn dev # Starts a Nodemon instance running the Adonis server on port 3333
```

// Service Worker

var CACHE_NAME = 'alarme-cache'
  const urlsToCache = [
    '/css/app.css',
    '/css/style.css',
    '/js/app.js'
  ]

self.addEventListener('install', event => {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME).then(cache => cache.addAll(urlsToCache))
  )
})

self.addEventListener('activate', () => {
  console.log('V1 now ready to handle fetches!')
})

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.match(event.request)
      .then(response => {
        // Cache hit - return response
        if (response) {
          return response
        }

        // IMPORTANT: Clone the request. A request is a stream and
        // can only be consumed once. Since we are consuming this
        // once by cache and once by the browser for fetch, we need
        // to clone the response.
        const fetchRequest = event.request.clone()

        return fetch(fetchRequest).then(
          response => {
            // Check if we received a valid response
            if(!response || response.status !== 200 || response.type !== 'basic') {
              return response
            }

            // IMPORTANT: Clone the response. A response is a stream
            // and because we want the browser to consume the response
            // as well as the cache consuming the response, we need
            // to clone it so we have two streams.
            const responseToCache = response.clone()

            caches.open(CACHE_NAME)
              .then(cache => {
                cache.put(event.request, responseToCache)
              })

            return response
          }
        )
      })
    )
})

// Set Actions
const actions = [
  {
    action: 'coffee-action',
    title: 'Coffee',
    icon: '/images/demos/action-1-128x128.png'
  },
  {
    action: 'doughnut-action',
    title: 'Doughnut',
    icon: '/images/demos/action-2-128x128.png'
  }
]

// Web Push
self.addEventListener('push', event => {
  const data = event.data.json();
  console.log('Got push', data);
  const promiseChain = self.registration.showNotification(`ALARM: ${data.alarm}`, {
    body: data.description,
    icon: 'https://mongoosejs.com/docs/images/mongoose5_62x30_transparent.png',
    requireInteraction: true,
    actions
  });
  event.waitUntil(promiseChain);
})

// Listen to clicks
self.addEventListener('notificationclick', function(event) {
  if (!event.action) {
    // Was a normal notification click
    const clickedNotification = event.notification;
    clickedNotification.close();
    console.log('Clicked notification');
  }

  switch (event.action) {
    case 'coffee-action':
      console.log('User ❤️️\'s coffee.');
      alert('User ❤️️\'s coffee.')
      break;
    case 'doughnut-action':
      console.log('User ❤️️\'s doughnuts.');
      break;
    default:
      console.log(`Unknown action clicked: '${event.action}'`);
      break;
  }
})

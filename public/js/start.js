if (!('serviceWorker' in navigator))  console.log(`Service Worker isn't supported on this browser`)
else registerServiceWorker()

// Register Service Worker
function registerServiceWorker() {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/js/sw.js')
      .then((registration) => {
        console.log('Service worker successfully registered.')
        return registration
      })
      .catch((err) => {
        console.error('Unable to register service worker.', err)
      })
  })
}

// Ask Permission to send user notifictions
// Called via a modal on the website
function askPermission() {
  if (!('PushManager' in window)) {
    console.log(`Push isn't supported on this browser`)
    return
  }
  return new Promise((resolve, reject) => {
    const permissionResult = Notification.requestPermission(result => {
      resolve(result)
    })
    if (permissionResult) {
      permissionResult.then(resolve, reject)
    }
  })
  .then(permissionResult => {
    if (permissionResult !== 'granted') {
      throw new Error('We weren\'t granted permission.')
    }
    // Now Subscribe the user
    subscribeUserToPush()
  })
}

// Function to correctly encode strings
function urlBase64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4)
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/')

  const rawData = window.atob(base64)
  return Uint8Array.from([...rawData].map((char) => char.charCodeAt(0)))
}

// Subscribe the user to push notifications
const subscribeUserToPush = async () => {
  navigator.serviceWorker.register('/js/sw.js').then(registration => {
    const subscribeOptions = {
      userVisibleOnly: true,
      applicationServerKey: urlBase64ToUint8Array(
        'BFSgLcX2_IpVvBnvv9nmuBDCwd7TxjCjiEcsYeq-0WHA9V5hObZBW4GZNOCClkV13WJ-KaUo-qz0AJRKXdz5QNM'
      )
    }
    return registration.pushManager.subscribe(subscribeOptions)
  })
  .then(pushSubscription => {
    // Now send the subscription to the back end
    sendSubscriptionToBackEnd(pushSubscription)
  })
  .catch((err) => {
    console.error('Unable to subscribe to push:', err)
  })
}

// Send subscription to the backend, and store in database
function sendSubscriptionToBackEnd(subscription) {
  return fetch('/subscription', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    credentials: 'same-origin',
    body: JSON.stringify(subscription)
  })
  .then(response => {
    if (response.status !== 200) {
      throw new Error('Bad status code from server.')
    }
    return response.json()
  })
  .then(responseData => {
    if (responseData.status !== 'success') {
      throw new Error(responseData.message)
    } else {
      console.log('Successfully registered subscription!')
      // Open Success Modal
      document.getElementById('successModal').style.display = 'block'
    }
  })
}

// Date Ops
function getFullTime(day, time) {
  const fullDate = date.parse(`${day} ${time}`, 'DD MMMM, YYYY HH:mm')
  return fullDate
}
